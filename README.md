# ATUI (Alex Technologies User Interface)
  
<p align="center">
  <img src="https://alcapitan.github.io/atui/patch/icons/logo.png" alt="Logo ATUI" width="200" height="200">
</p>
<p align="center">Framework web pour créer de nombreux types de sites web, facilement est simplement.</p>

Les objectifs du framework sont une simplicité d'utilisation pour le grand public, une performance optimale et une compatibilité fiable envers les navigateurs récents (Chrome, Edge, Firefox, Safari, Opera, ect... et dans des versions datant de moins de 2 ans).  
Ainsi, ATUI est conçu dans l'esprit de s'adapter aux connexions internet faibles et aux appareils avec de mauvaises performances).  

## Attention ! Informations développement !
ATUI est un projet qui est encore en phase de développement. Ainsi, il est déconseillé d'utiliser ce projet pour l'instant.  
Si vous êtes curieux de découvrir ATUI, d'abord je vous en remercie de l'interêt porté !  
La branche actuelle nommée ```beta``` contient le travail suite à la seconde phase de beta ! Pour voir les avancés de conception les plus récentes, <a href="https://github.com/alcapitan/atui/tree/dev">veuillez aller vers la branche ```dev``` !</a> 
  
Comme écrit ci-dessus, ATUI est un projet en développement. Rassurez-vous, à l'avenir, le projet bénéficira d'une documentation complète ! De plus, sachez que la première version stable sera dans la future branche ```lts```.

<p align="center">
    <img src="/patch/documents/organisation branches.png" alt="Organisation des branches jusqu'à la première version stable. Daté du 30/10/21." width="720" height="405">
</p>
  
## Télécharger le projet (avec Git)

### Dans la version beta (version la plus stable disponible à ce jour mais incomplète pour le grand public)
```git clone https://github.com/alcapitan/atui.git beta```
### Dans la version dev (version la plus récente mais la plus instable)
```git clone https://github.com/alcapitan/atui.git dev```

## Comment l'utiliser ?

Comme dit ci-dessus, le projet est en développement. Par conséquent, je ne peux pas faire de documentation fiable car l'utilisation n'est aujourd'hui pas ouverte au grand public.  
  
Ceci dit, ATUI est conçu de manière à fractionner votre site web final de la manière suivante : le dossier ```patch``` (c'est à dire toutes les données nécéssaires pour créer votre site à partir d'ATUI) et le dossier ```atui``` comportant le ```kernel``` (le noyau de fonctionnement d'ATUI) ainsi que autant d'extensions dont l'utilisateur à besoin.  
  
En effet, ATUI offre la possibilité d'intégrer des extensions à son site web pour ajouter diverses fonctionnalités supplémentaires. Avec ATUI, je propose des extensions mais chaque développeur est libre d'en créer de la manière qu'il veut. A la condition de respecter les normes de fragmentation des variables (pas de documentation disponible à ce sujet là mais allez voir le code CSS par exemple et vous comprendrez comment la norme des variables fonctionne).
  
## Contribuer au projet
  
ATUI est évidemment un projet open-source et compte bien profiter de votre aide pour mieux avancer.  
N'hésitez pas à m'envoyer vos signalements de bogues, idées de nouvelles fonctionnalités ou d'amélioration de fonctionnalités existantes et vos questions, dans la rubrique <a href="https://github.com/alcapitan/atui/issues">Issues</a>, cela m'aiderai beaucoup dans le développement.  
De plus, comme écrit ci-dessus, vous pouvez créer vos propres extensions à associer avec le kernel d'ATUI.
  
## Licence & Utilisation de service externe

License du projet : <a href="https://github.com/alcapitan/atui/blob/public/LICENSE.md">GNU General Public Licence</a>

Sites web externes utilisés pour concevoir ATUI : 
- Apprendre la programmation : <a href="https://www.w3schools.com/">W3Schools</a>, <a href="https://openclassrooms.com/fr/">OpenClassRooms</a>
- Connaitre la compatibilité des navigateurs web envers différentes notions de programmation : <a href="https://caniuse.com/">Caniuse</a>
- Icones : <a href="https://icones8.fr/">Icones8</a>
- Illustrations : <a href="https://www.pexels.com/fr-fr/">Pexels</a>
- Mesure des performances et conseils d'amélioration : <a href="https://developers.google.com/speed/pagespeed/insights/?hl=fr">Google Insights</a>
- Polices d'écriture : <a href="https://fonts.google.com/">Google Fonts</a>
- Service web d'hébergement et de gestion de développement : <a href="https://github.com/">GitHub</a>
- Vérification du code HTML : https://validator.w3.org/
- Vérificateur du code CSS : https://jigsaw.w3.org/css-validator/
  
Logiciels utilisés pour concevoir ATUI : 
- Editeur de code : <a href="https://code.visualstudio.com/">Visual Studio Code 2019</a>
- Version control : <a href="https://git-scm.com/">Git</a>
  